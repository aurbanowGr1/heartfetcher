package repositories;

import domain.User;


public class DaoPatternDemo {

	public static void main(String[] args) {

		  UserDao userDao = new UserDaoImpl();

	      //print all students
	      for (User user : userDao.getAllUsers()) {
	    	  System.out.println("User: [Id : "
	    		         +user.getId()+", Login : "+user.getLogin() + " Age: " +user.getAge() + " " + "Email: " +user.getEmail() + " " + "Pw:" +user.getPassword() + " " + "Personality: " +user.getPersonality() +   " ]");	
	    			
	    			
	      }


	      //update student
	      User user = userDao.getAllUsers().get(0);
	      user.setLogin("Michael");
	      userDao.updateUser(user);

	      //get the student
	      userDao.getUser(0);
	      System.out.println("User: [Id : "
	         +user.getId()+", Login : "+user.getLogin() + " Age: " +user.getAge() + " " + "Email: " +user.getEmail() + " " + "Pw:" +user.getPassword() + " " + "Personality: " +user.getPersonality() +   " ]");	
		
		
		
	}

}
