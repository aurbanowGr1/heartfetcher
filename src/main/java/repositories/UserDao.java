package repositories;

import java.util.List;

import domain.User;

public interface UserDao {

	   public List<User> getAllUsers();
	   public User getUser(int ID);
	   public void updateUser(User user);
	   public void deleteUser(User user);
	
}
