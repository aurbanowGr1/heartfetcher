package repositories;

	import java.util.ArrayList;
	import java.util.List;
	import domain.User;
	public class UserDaoImpl implements UserDao {

		
	   //list is working as a database
	   List <User> users;

	   public UserDaoImpl(){
	      users = new ArrayList<User>();
	      User user1 = new User("Robert","pw", "@", false, 30, 1, null);
	      User user2 = new User("John","pw", "@", true, 25, 2, null);
	      users.add(user1);
	      users.add(user2);		
	   }
	   @Override
	   public void deleteUser(User user) {
	      users.remove(user.getId());
	      System.out.println("User: ID " + user.getId() 
	         +", deleted from database");
	   }

	   //retrive list of Users from the database
	   @Override
	   public List<User> getAllUsers() {
	      return users;
	   }

	   @Override
	   public User getUser(int ID) {
	      return users.get(ID);
	   }

	   @Override
	   public void updateUser(User user) {
	      users.get(user.getId()).setLogin(user.getLogin());
	      System.out.println("User: Id " + user.getId() 
	         +", updated in the database");
	   }
	}
	

