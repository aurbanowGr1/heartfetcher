
package domain;


public class Personality 
{
    
    int x;
    int y;
    int character;
 
    public Personality(int x, int y, int character) {
         this.x = x;
         this.y = y;
         this.character = character;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getCharacter() {
        return character;
    }

    public void setCharacter(int character) {
        this.character = character;
    }

    


}
