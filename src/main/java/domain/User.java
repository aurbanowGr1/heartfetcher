
package domain;

public class User 
{

    String login;
    String password;
    String email;
    boolean sex;
    int age;
    int id;
 

	Personality personality;
    
 

    public User(String login, String password, String email, boolean sex,
			int age, int id, Personality personality) {
		super();
		this.login = login;
		this.password = password;
		this.email = email;
		this.sex = sex;
		this.age = age;
		this.id = id;
		this.personality = personality;
	}

	
    public int getId() {
 		return id;
 	}

 	public void setId(int id) {
 		this.id = id;
 	}
 	
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Personality getPersonality() {
        return personality;
    }

    public void setPersonality(Personality personality) {
        this.personality = personality;
    }
  


} 
