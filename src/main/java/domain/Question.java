

package domain;


public class Question 
{
    
    String q;
    int modifx;
    int modify;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public int getModifx() {
        return modifx;
    }

    public void setModifx(int modifx) {
        this.modifx = modifx;
    }

    public int getModify() {
        return modify;
    }

    public void setModify(int modify) {
        this.modify = modify;
    }
}
